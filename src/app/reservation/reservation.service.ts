import { Injectable } from '@angular/core';
import { NewReservationComponent } from './new-reservation/new-reservation.component';
import { Reservation } from './reservation.model';
import { Restaurant } from '../restaurant/restaurant.model';
import { AuthService } from '../auth/auth.service';
import { BehaviorSubject } from 'rxjs';
import { take , delay , tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor(private authService: AuthService) { }

  private _reservations = new BehaviorSubject<Reservation[]>([]);

  get reservations() {
    return this._reservations.asObservable();
  }

  newReservation(
    restaurantId: string,
    restaurant_title: string,
    reservation_datetime: Date
  ) {
    const addReservation = new Reservation(
      Math.random().toString(),
      restaurantId,
      this.authService.userId,
      restaurant_title,
      reservation_datetime
    );

    return this.reservations.pipe(
      take(1),
      delay(1000),
      tap( reservations => {
        this._reservations.next(reservations.concat(addReservation));
      }))
  }

  deleteReservation(reservationId: string) {
    return this.reservations.pipe(
      take(1),
      delay(1000),
      tap( reservations => {
        this._reservations.next(reservations.filter( rs => rs.id !== reservationId));
      })
    )
  }

}


