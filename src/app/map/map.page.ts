import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Restaurant } from '../restaurant/restaurant.model';
import { RestaurantService } from '../restaurant/restaurant.service';

declare var google: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {
  loadedRestaurants: Restaurant[];

  map: any;
  @ViewChild('map', { read: ElementRef, static: false}) mapRef: ElementRef;

  infoWindows: any = [];

  constructor(private restaurantService: RestaurantService) { }

  ngOnInit() {
    this.loadedRestaurants = this.restaurantService.restaurants;
  }

  addMarkersToMap(markers) {
    for(let marker of markers) {
      let position = new google.maps.LatLng(marker.latitude, marker.longitude);
      let mapMarker = new google.maps.Marker({
        position: position,
        name: marker.name,
        latitude: marker.latitude,
        longitude: marker.longitude
      });

      mapMarker.setMap(this.map);
      this.addInfoWindowToMarker(mapMarker);
    }
  }

  addInfoWindowToMarker(marker) {
    let infoWindowContent = '<div><p>'+ marker.name +'</p></div>';

    let infoWindow = new google.maps.InfoWindow({
      content: infoWindowContent
    });

    marker.addListener('click', () => {
      this.closeAllInfoWindows();
      infoWindow.open(this.map, marker);
    });
    this.infoWindows.push(infoWindow);
  }

  closeAllInfoWindows() {
    for(let window of this.infoWindows) {
      window.close();
    }
  }

  showMap() {
    const location = new google.maps.LatLng(6.5482947,101.2877865);
    const options = {
      center: location,
      zoom: 15,
      disableDefaultUI: true
    }
    this.map = new google.maps.Map(this.mapRef.nativeElement, options);
    this.addMarkersToMap(this.loadedRestaurants);
  }

  ionViewDidEnter() {
    this.showMap();
  }

}
